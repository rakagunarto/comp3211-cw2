// env var: AUTH_SERVICE_URL

const MAX_MESSAGES = 50
const MESSAGES_KEY = 'messages'

addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

function json(object, status) {
    const str = JSON.stringify(object)
    status = status || 200
    return new Response(str, {
        status,
        headers: {
            'content-type': 'application/json',
            'access-control-allow-origin': '*',
        },
    })
}

async function sendMessage(jwt, user, type, content) {
    const authResult = await fetch(AUTH_SERVICE_URL, {
        method: 'POST',
        body: `{"token": "${jwt}"}`,
    })
    if (!authResult.ok) return json({ result: 'bad_auth' }, 401)

    console.log('message:', user, type, content)
    try {
        let messages = JSON.parse(await MESSAGES.get(MESSAGES_KEY)) || []
        messages.push([Date.now(), user, type, content])
        if (messages.length > MAX_MESSAGES) messages = messages.shift()
        await MESSAGES.put(MESSAGES_KEY, JSON.stringify(messages))
        return json({ result: 'ok' })
    } catch (e) {
        console.log(e.message)
        return json({ result: 'error', why: JSON.stringify(e, null, 2) })
    }
}

async function getMessages() {
    let messages = JSON.parse(await MESSAGES.get(MESSAGES_KEY))
    //messages = JSON.stringify(messages)
    return json({ result: 'ok', messages })
}

/**
 * Respond with hello worker text
 * @param {Request} request
 */
async function handleRequest(request) {
    const action = new URL(request.url).pathname

    //const contentType = request.headers.get('content-type')
    //if (!contentType.includes('application/json'))
    //    return json({ result: 'bad_content' })
    if (action !== '/messages') return json({ result: 'bad_action' })
    if (request.method === 'POST') {
        const { jwt, user, type, content } = await request.json()
        return sendMessage(jwt, user, type, content)
    } else if (request.method === 'GET') {
        return getMessages()
    } else if (request.method === 'OPTIONS') {
        return new Response('', {
            status: 204,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type',
            },
        })
    } else {
        return json({ result: 'bad_action' })
    }
}
