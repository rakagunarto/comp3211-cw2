/**
 * @module auth
 * @requires itty-router
 * @requires jsonwebtoken
 */
import { Router } from 'itty-router'
import * as jwt from 'jsonwebtoken'

/**
 * @type {object}
 * @constant
 * @namespace router
 */
const router = Router()

/**
 * @type {object}
 * @constant
 */
const defaultHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
}

router.routes.push([
    'OPTIONS',
    /.*/,
    [
        async request => {
            return new Response(null, {
                status: 204,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods':
                        'GET, POST, DELETE, OPTIONS',
                    'Access-Control-Allow-Headers': 'Content-Type',
                },
            })
        },
    ],
])

/**
 * Logs in, a JWT will be returned if a record for that username is not found
 * The account is ephemeral, only lasting one hour
 * The username will be automatically released after that time and the session
 * is automatically invalidated
 *
 * Expects a JSON object with a username field
 * @name POST_/login
 * @function
 * @memberof module:auth~router
 * @inner
 */
router.post('/login', async request => {
    // retrieve username
    let { username } = await request.json()
    if (!username)
        return new Response('', { status: 400, headers: defaultHeaders })

    // check if the username already has a record
    if (await USERS.get(username))
        return new Response('', { status: 400, headers: defaultHeaders })

    // create the token
    const token = jwt.sign(
        {
            username,
        },
        SECRET_KEY,
        {
            expiresIn: parseInt(EPHERMERAL_DURATION),
        }
    )

    // create record
    await USERS.put(username, true, {
        expirationTtl: parseInt(EPHERMERAL_DURATION),
    })

    // return token
    return new Response(JSON.stringify(token), {
        status: 200,
        headers: defaultHeaders,
    })
})

/**
 * Nukes a session (request of dylan :D)
 *
 * @name DELETE_/session
 * @function
 * @memberof module:auth~router
 * @inner
 */
router.delete('/session', async request => {
    // token
    const { token } = await request.json()
    if (!token)
        return new Response('', { status: 400, headers: defaultHeaders })

    // attempt to verify token
    let decoded
    try {
        decoded = jwt.verify(token, SECRET_KEY)
    } catch {
        return new Response('', { status: 401, headers: defaultHeaders })
    }

    // nuke it
    await USERS.delete(decoded.username)

    return new Response('token boom', {
        status: 200,
        headers: defaultHeaders,
    })
})

/**
 * Confirms whether a token is a valid token or not
 *
 * Expects a JSON object with a token field
 * @name POST_/verify
 * @function
 * @memberof module:auth~router
 * @inner
 */
router.post('/verify', async request => {
    // retrieve token
    const { token } = await request.json()
    if (!token)
        return new Response('', { status: 400, headers: defaultHeaders })

    // attempt to verify token
    try {
        jwt.verify(token, SECRET_KEY)
    } catch {
        return new Response('', { status: 401, headers: defaultHeaders })
    }

    return new Response('', { status: 200, headers: defaultHeaders })
})

// send 404s to other routes
router.all(
    '*',
    () =>
        new Response('404, not found!', {
            status: 404,
        })
)

// register event listener to use router on requests
addEventListener('fetch', e => {
    e.respondWith(router.handle(e.request))
})
